<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
    </style>

    <head>
        <title>Tasks Of Project</title>
    </head>

    <header class="header"></header>

    <body>

        <h1>TASK MANAGEMENT</h1>


        <table border="3" cols="4" width="100%" style="border-collapse: collapse">
            <tr>
                <td width="20%">TASK ID</td>
                <td width="20%">TASK NAME</td>
                <td width="20%">TASK DESCRIPTION</td>
                <td width="10%">EDIT</td>
                <td width="10%">REMOVE</td>
            </tr>
            <c:forEach items="${tasks}" var="task">
                <tr>
                    <td width="20%">${task.id}</td>
                    <td width="20%">${task.name}</td>
                    <td width="20%">${task.description}</td>
                    <td width="10%"><a href="/tasks/editForm/${task.id}">EDIT</a></td>
                    <td width="10%"><a href="/tasks/remove?taskId=${task.id}&projectId=${projectId}">REMOVE</a></td>
                </tr>
            </c:forEach>
        </table>
        <div><br></div>

        <form action=${pageContext.request.contextPath}/tasks/createForm?projectId=${projectId}">
            <input type="hidden" name="projectId" value="${projectId}">
            <input type="submit" value="CREATE TASK" class="button">
        </form>

        <form action="/projects/all">
            <input type="submit" value="BACK TO PROJECTS" class="customButton">
        </form>

    </body>

</html>
