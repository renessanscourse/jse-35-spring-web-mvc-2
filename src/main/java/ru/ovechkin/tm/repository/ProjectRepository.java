package ru.ovechkin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}