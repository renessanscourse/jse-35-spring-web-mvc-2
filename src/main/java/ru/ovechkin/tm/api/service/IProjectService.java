package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    void save(@Nullable Project project);

    void removeById(@Nullable String projectId);

    Project findById(@Nullable String projectId);

    @Transactional
    void updateById(@Nullable String id, @Nullable Project project);
}